Name:           hunspell-kk
Summary:        Kazakh hunspell dictionaries for hunspell used in OpenOffice.
Version:        1.1
Release:        16
Source:         https://downloads.sourceforge.net/project/aoo-extensions/1172/12/dict-kk.oxt
URL:            http://extensions.services.openoffice.org/project/dict-kk
License:        GPLv2+ or LGPLv2+ or MPLv1.1
BuildArch:      noarch
Requires:       hunspell
Supplements:    (hunspell and langpacks-kk)

%description
The package provide hunspell dictionaries for kazakh language.

%prep
%autosetup -c -n hunspell-kk -p1 -S git

%build
tr -d '\r' < README_kk_KZ.txt > README_kk_KZ.txt.new;touch -r README_kk_KZ.txt README_kk_KZ.txt.new
mv -f README_kk_KZ.txt.new README_kk_KZ.txt

%install
install -d $RPM_BUILD_ROOT/%{_datadir}/myspell
install -p kk_KZ.* $RPM_BUILD_ROOT/%{_datadir}/myspell

%files
%doc README_kk_KZ.txt
%{_datadir}/myspell/*

%changelog
* Sat Apr 18 2020 Jeffery.Gao <gaojianxign.huawei.com> - 1.1-16
- Package init
